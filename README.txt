
This module provides an IRC action to Drupal's core. Requires Token and Bot
modules.

NOTE: Installation currently requires a patched version of the Bot module.
Until the patch is applied to Bot's HEAD, please refer to the Bot Actions
project page on drupal.org for details on patching Bot.

Written and maintained by:
Sean Edwards
Not So Random
http://www.notsorandom.com/

